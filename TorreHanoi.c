#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <Windows.h>
#include <time.h>

typedef struct pilhaEstatica
{
    int pilha[5];
    int topoPilha;
} pilha;

void push(pilha *pPilha, int pValor);

int pop(pilha *pPilha);

int size(pilha *pPilha);

int stackpop(pilha *pPilha);

void printMenu();

void printJogo(pilha pTorreA, pilha pTorreB, pilha pTorreC);

bool moverDisco(pilha *pTorre1, pilha *pTorre2);

void atualizarMelhoresTempos(int minutos, int segundos);

void printMelhoresTempos();

void jogarManual();

void jogarAuto();

// Variáveis globais
//                           m  s  t (total)
int melhoresTempos[5][3] = {{0, 0, 999999},
                            {0, 0, 999999},
                            {0, 0, 999999},
                            {0, 0, 999999},
                            {0, 0, 999999}};

int posPiorTempo = 0;

int resolucaoHanoi[32] = {2, 1, 6, 2, 3, 4, 2, 1, 6, 5, 3, 6, 2, 1, 6, 2, 3, 4, 2, 3, 6, 5, 3, 4, 2, 1, 6, 2, 3, 4, 2};

int main()
{
    int opcao = 0;

    while(opcao != 4)
    {
        printMenu();
        printf("\n");

        scanf("%d", &opcao);
        system("cls");

        switch (opcao)
        {
            // Manual
            case 1:
                jogarManual();
                break;

            // Automático
            case 2:
                jogarAuto();
                break;

            // Rank
            case 3:
                /* Testar a função 
                atualizarMelhoresTempos(25, 31);
                atualizarMelhoresTempos(11, 11);
                atualizarMelhoresTempos(13, 53);
                atualizarMelhoresTempos(22, 33);
                atualizarMelhoresTempos(33, 1);
                atualizarMelhoresTempos(15, 23);
                atualizarMelhoresTempos(20, 41);
                */

                printMelhoresTempos();
                break;

            // Encerrar
            case 4:
                break;
            
            default:
                printf("\nOpcao invalida!\n");
                Sleep(500);
                break;
        }

        system("cls");
    }

    return 0;
}

void push(pilha *pPilha, int pValor)
{
    if(pPilha->topoPilha++ < 5)
    {
        pPilha->pilha[pPilha->topoPilha] = pValor;
    }
    else
    {
        pPilha->topoPilha--;
        printf("\nErro: A pilha estah cheia!\n");
    }
}

int pop(pilha *pPilha)
{
    int topo = pPilha->topoPilha;
    
    if(topo < 0)
    {
        return -1;
    }

    pPilha->topoPilha--;
    return pPilha->pilha[topo];
}

int size(pilha *pPilha)
{
    return pPilha->topoPilha;
}

int stackpop(pilha *pPilha)
{
    return pPilha->pilha[pPilha->topoPilha];
}

void printMenu()
{
    printf("1 - Jogar manual\n");
    printf("2 - Jogar automatico\n");
    printf("3 - Listar os melhores tempos\n");
    printf("4 - Encerrar\n");
}

void printJogo(pilha pTorreA, pilha pTorreB, pilha pTorreC)
{
    printf("  |      |      |  \n");
    printf("  |      |      |  \n");

    printf("  ");
    if(size(&pTorreA) == 4)
        printf("%d", pop(&pTorreA));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreB) == 4)
        printf("%d", pop(&pTorreB));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreC) == 4)
        printf("%d", pop(&pTorreC));
    else
        printf("|");
    printf("  \n");

    printf("  ");
    if(size(&pTorreA) == 3)
        printf("%d", pop(&pTorreA));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreB) == 3)
        printf("%d", pop(&pTorreB));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreC) == 3)
        printf("%d", pop(&pTorreC));
    else
        printf("|");
    printf("  \n");

    printf("  ");
    if(size(&pTorreA) == 2)
        printf("%d", pop(&pTorreA));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreB) == 2)
        printf("%d", pop(&pTorreB));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreC) == 2)
        printf("%d", pop(&pTorreC));
    else
        printf("|");
    printf("  \n");

    printf("  ");
    if(size(&pTorreA) == 1)
        printf("%d", pop(&pTorreA));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreB) == 1)
        printf("%d", pop(&pTorreB));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreC) == 1)
        printf("%d", pop(&pTorreC));
    else
        printf("|");
    printf("  \n");

    printf("  ");
    if(size(&pTorreA) == 0)
        printf("%d", pop(&pTorreA));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreB) == 0)
        printf("%d", pop(&pTorreB));
    else
        printf("|");
    printf("      ");

    if(size(&pTorreC) == 0)
        printf("%d", pop(&pTorreC));
    else
        printf("|");
    printf("  \n");

    printf("-----  -----  -----\n");
    printf("  A      B      C  \n");
}

/* 
    pTorre1 é a torre com o disco a ser movido, pTorre2 é o destino.
    A função retorna true se o disco foi movido, e retorna false se o disco não foi movido.
*/
bool moverDisco(pilha *pTorre1, pilha *pTorre2)
{
    // Se a torre possui algum disco para mover
    if(size(pTorre1) >= 0)
    {
        // Se a torre destino possui algum disco para comparar
        if(size(pTorre2) >= 0)
        {
            // Comparar se o disco da torre 1 é maior do que o disco da torre destino
            if(stackpop(pTorre1) > stackpop(pTorre2))
            {
                return false;
            }
            else
            {
                push(pTorre2, pop(pTorre1));
                return true;
            }
        }
        else
        {
            push(pTorre2, pop(pTorre1));
            return true;
        }
    }
    else
        return false;
}

// Confere se a torre está completa
bool gameover(pilha pTorre)
{
    if(size(&pTorre) == 4)
        return true;
    return false;
}

// Atualiza a lista de melhores tempos
void atualizarMelhoresTempos(int minutos, int segundos)
{
    int tempoTotal = minutos * 60 + segundos;

    if(tempoTotal < melhoresTempos[posPiorTempo][2])
    {
        int i, auxMin, auxSec, auxTot;

        for(i = posPiorTempo; i >= 0; i--)
        {
            if(tempoTotal < melhoresTempos[i][2])
            {
                // Realiza a troca no rank
                auxMin = melhoresTempos[i][0];
                auxSec = melhoresTempos[i][1];
                auxTot = melhoresTempos[i][2];

                melhoresTempos[i][0] = minutos;
                melhoresTempos[i][1] = segundos;
                melhoresTempos[i][2] = tempoTotal;

                melhoresTempos[i + 1][0] = auxMin;
                melhoresTempos[i + 1][1] = auxSec;
                melhoresTempos[i + 1][2] = auxTot;
            }
        }
    }

    // Conferir se existe um espaço vazio na lista
    if(posPiorTempo < 4)
    {
        posPiorTempo++;
    }
}

void printMelhoresTempos()
{
    int i;

    printf("    m  s\n");

    for(i = 0; i < 5; i++)
    {
        printf("%d - %d  %d\n", i + 1, melhoresTempos[i][0], melhoresTempos[i][1]);
    }

    printf("\n");
    system("pause");
}

void jogarManual()
{
    pilha torreA, torreB, torreC;
    int opcao;

    // Inicializar valores
    torreA.topoPilha = -1;
    torreB.topoPilha = -1;
    torreC.topoPilha = -1;

    push(&torreA, 5);
    push(&torreA, 4);
    push(&torreA, 3);
    push(&torreA, 2);
    push(&torreA, 1);

    // Marcar o tempo de inicio do jogo
    time_t tempo = time(NULL); // time(NULL) retorna o tempo do S.O.
    struct tm tempoInicio = *localtime(&tempo);

    while (!gameover(torreC))
    {
        printJogo(torreA, torreB, torreC);

        printf("___________________\n\n");

        // Menu
        printf("1 - Mover disco do pino A para o pino B\n");
        printf("2 - Mover disco do pino A para o pino C\n");
        printf("3 - Mover disco do pino B para o pino A\n");
        printf("4 - Mover disco do pino B para o pino C\n");
        printf("5 - Mover disco do pino C para o pino A\n");
        printf("6 - Mover disco do pino C para o pino B\n\n");

        scanf("%d", &opcao);
        system("cls");

        switch (opcao)
        {
            // A -> B
            case 1:
                if(!moverDisco(&torreA, &torreB))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // A -> C
            case 2:
                if(!moverDisco(&torreA, &torreC))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // B -> A
            case 3:
                if(!moverDisco(&torreB, &torreA))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // B -> C
            case 4:
                if(!moverDisco(&torreB, &torreC))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // C -> A
            case 5:
                if(!moverDisco(&torreC, &torreA))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // C -> B
            case 6:
                if(!moverDisco(&torreC, &torreB))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;
            
            default:
                printf("\nOpcao invalida!\n");
                Sleep(500);
                system("cls");
                break;
        }
    }

    // Marcar o tempo do fim do jogo
    tempo = time(NULL);
    struct tm tempoFim = *localtime(&tempo);

    // Calcular o tempo
    int minuto, segundo;

    if(tempoInicio.tm_hour == tempoFim.tm_hour)
    {
        minuto = tempoFim.tm_min - tempoInicio.tm_min;
    }
    else
    {
        if(tempoInicio.tm_min > tempoFim.tm_min)
            minuto = tempoInicio.tm_min - tempoFim.tm_min;
        else
            minuto = tempoFim.tm_min - tempoInicio.tm_min;
    }

    if(tempoInicio.tm_sec > tempoFim.tm_sec)
        segundo = tempoInicio.tm_sec - tempoFim.tm_sec;
    else
        segundo = tempoFim.tm_sec - tempoInicio.tm_sec;

    atualizarMelhoresTempos(minuto, segundo);

    // Mostrar resultado
    printJogo(torreA, torreB, torreC);
    printf("___________________\n\n");
    
    printf("\nYOU WIN!\n\n");
    printf("Seu tempo foi de: %d minutos e %d segundos!\n\n", minuto, segundo);

    system("pause");
}

void jogarAuto()
{
    pilha torreA, torreB, torreC;
    int resposta, i = 0;

    // Inicializar valores
    torreA.topoPilha = -1;
    torreB.topoPilha = -1;
    torreC.topoPilha = -1;

    push(&torreA, 5);
    push(&torreA, 4);
    push(&torreA, 3);
    push(&torreA, 2);
    push(&torreA, 1);

    while (!gameover(torreC))
    {
        printJogo(torreA, torreB, torreC);
        printf("___________________\n\n");

        resposta = resolucaoHanoi[i++];
        Sleep(1000);

        system("cls");

        switch (resposta)
        {
            // A -> B
            case 1:
                if(!moverDisco(&torreA, &torreB))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // A -> C
            case 2:
                if(!moverDisco(&torreA, &torreC))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // B -> A
            case 3:
                if(!moverDisco(&torreB, &torreA))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // B -> C
            case 4:
                if(!moverDisco(&torreB, &torreC))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // C -> A
            case 5:
                if(!moverDisco(&torreC, &torreA))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;

            // C -> B
            case 6:
                if(!moverDisco(&torreC, &torreB))
                {
                    printf("\nMovimento invalido!\n");
                    Sleep(500);
                    system("cls");
                }
                break;
            
            default:
                printf("\nOpcao invalida!\n");
                Sleep(500);
                system("cls");
                break;
        }
    }

    // Mostrar resultado
    printJogo(torreA, torreB, torreC);
    printf("___________________\n\n");

    printf("\nMovimentos totais: 31!\n\n");
    system("pause");
}