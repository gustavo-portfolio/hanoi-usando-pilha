# Hanoi usando pilha

## Apresentação

Na disciplina de Estrutura de Dados I, o professor desafiou a turma a criar o jogo "Torre de Hanoi" com 5 discos utilizando a estrutura de pilha para representar as torres, o jogador pode escolher as opções:

* Manual: Para tentar resolver a torre de hanoi (seu tempo deve ser armazenado).

* Automático: Para que a "máquina jogue" e mostre o caminho otimizado.

* Listar os melhores tempos: Para saber quais foram os seus 5 melhores tempos.

<strong>Esse jogo foi desenvolvido em 1 dia</strong>